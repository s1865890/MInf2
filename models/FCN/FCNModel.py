# © All rights reserved. University of Edinburgh, United Kingdom
# SMILE Project, 2023

import numpy as np
import pandas as pd

from pathlib import Path
import matplotlib.pyplot as plt

from keras.models import Model
from keras.layers import Conv1D, Reshape, Input, Multiply, Lambda
from keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger, ReduceLROnPlateau



DEFAULT_BATCH_SIZE = 256
DEFAULT_NUM_EPOCHS = 200
DEFAULT_NUM_DILATION_LAYERS = 8

MAINS_REAL = 'mains_real'
MAINS_APPARENT = 'mains_apparent'

class MyModelCheckpoint(ModelCheckpoint):
    def set_model(self, model):
        pass

class NILM_FCN_Model(object):

    def __init__(self, appliance,
                 num_dilation_layers=DEFAULT_NUM_DILATION_LAYERS,
                 dilated_filter_size=3, initial_filter_size=9,
                 num_filters=128, frequency='10S',
                 prediction_lag=None, typeOfPower = 'mains_apparent'):


        self.appliance = appliance
        self.num_dilation_layers = num_dilation_layers
        self.dilated_filter_size = dilated_filter_size
        self.initial_filter_size = initial_filter_size
        self.num_filters = num_filters
        self.frequency = frequency
        self.prediction_lag = prediction_lag
        self.typeOfPower = typeOfPower

        self.model = None
        self.input_window_length = None
        self.output_window_length = None

        self.appliance_stats = pd.read_csv('appliance_stats.csv')
        self.mean_on_power = float(self.appliance_stats[self.appliance_stats['Unnamed: 0']==appliance].mean_on_power.values[0])
        self.std_on_power = float(self.appliance_stats[self.appliance_stats['Unnamed: 0']==appliance].std_on_power.values[0])

    def init_model(self, window_length=None):
        """ Initialise the model architecture. """

        # Set the input and output window length
        min_length = self.get_minimum_input_length()
        if window_length is None:
            # use the default size
            input_window_length = 2 * min_length - 1
        elif window_length == 0:
            # use the smallest allowed window
            input_window_length = min_length
        elif window_length < 1:
            # define the output window length
            input_window_length = min_length - 1 - window_length
        else:
            # define the input window length
            input_window_length = window_length
        if input_window_length < min_length:
            raise ValueError(f'The input window length must be at least {min_length}.')

        output_window_length = input_window_length//2
        self.input_window_length = input_window_length
        self.output_window_length = output_window_length

        offset = (self.input_window_length - self.output_window_length)//2

        # define model
        main_input = Input(shape=(input_window_length,), name='main_input')
        x = Reshape((input_window_length, 1),  input_shape=(input_window_length,))(main_input)

        x = Conv1D(128, 9, padding='same', activation='relu', dilation_rate=1)(x)
        x = Conv1D(128, 3, padding='same', activation='relu', dilation_rate=2)(x)
        x = Conv1D(128, 3, padding='same', activation='relu', dilation_rate=4)(x)
        x = Conv1D(128, 3, padding='same', activation='relu', dilation_rate=8)(x)
        x = Conv1D(128, 3, padding='same', activation='relu', dilation_rate=16)(x)
        x = Conv1D(128, 3, padding='same', activation='relu', dilation_rate=32)(x)
        x = Conv1D(128, 3, padding='same', activation='relu', dilation_rate=64)(x)
        x = Conv1D(128, 3, padding='same', activation='relu', dilation_rate=128)(x)
        x = Conv1D(128, 3, padding='same', activation='relu', dilation_rate=256)(x)
        x = Conv1D(256, 1, padding='same', activation='relu')(x)
        x = Conv1D(1, 1, padding='same', activation=None)(x)

        x = Reshape((input_window_length,),  input_shape=(input_window_length,1))(x)

        x = Lambda(lambda x: x[:, offset:-offset], output_shape=(output_window_length,))(x)

        targets_mask = Input(shape=(output_window_length,), name='targets_mask')
        main_output = Multiply()([x, targets_mask])
        single_model = Model(inputs=[main_input, targets_mask], outputs=[main_output])

        # summary
        single_model.summary()
        assert x.shape[1] == output_window_length


        # Define the target mask input
        targets_mask = Input(shape=(output_window_length,), name='targets_mask')

        # Mask the parts of the output which are deemed unsuitable (as defined by the input)
        main_output = Multiply()([x, targets_mask])
        print(main_output.shape)
        # Create the Model architecture
        single_model = Model(inputs=[main_input, targets_mask], outputs=[main_output], name=self.appliance+'_'+self.typeOfPower)

        return single_model

    # Use negative numbers for actual input window sizes, and positive numbers for "batch" sizes.
    def train(self, training_data, validation_data=None,
              num_epochs=DEFAULT_NUM_EPOCHS, batch_size=DEFAULT_BATCH_SIZE,
              window_length=None, pad_final_window=True,
              mask_underspecified_targets=True, drop_masked_windows=True,
              logging_path=None, model_path=None, verbose=0, **kwargs):
        """ Train the model. """

        model = self.init_model(window_length)

        if verbose:
            print(f'Preparing training data', flush=True)
        x_train, x_train_mask, y_train, _ = self.prepare_data(training_data,
                                             pad_final_window=pad_final_window,
                                             mask_underspecified_targets=mask_underspecified_targets,
                                             drop_masked_windows=drop_masked_windows,
                                             verbose=verbose,
                                             **kwargs)
        if y_train is None:
            raise ValueError('Training data must have targets.')
        if validation_data is None:
            monitor = 'loss'
        else:
            if verbose:
                print(f'Preparing validation data', flush=True)
            x_valid, x_valid_mask, y_valid, _ = self.prepare_data(validation_data,
                                                 pad_final_window=pad_final_window,
                                                 mask_underspecified_targets=mask_underspecified_targets,
                                                 drop_masked_windows=drop_masked_windows,
                                                 verbose=verbose,
                                                 **kwargs)
            if y_valid is None:
                raise ValueError('Validation data must have targets.')
            monitor = 'val_loss'
            validation_data = (x_valid, y_valid)
        print("MAX train: ", np.nanmax(y_train))
        print("MAX valid: ", np.nanmax(y_valid))

        mean_power = self.appliance_stats[self.appliance_stats['Unnamed: 0']==self.typeOfPower.split('_')[1]].mean_on_power.values[0]
        std_power = self.appliance_stats[self.appliance_stats['Unnamed: 0']==self.typeOfPower.split('_')[1]].std_on_power.values[0]
        # Normalise the values
        x_train = (x_train-x_train.mean(axis=1).reshape((-1,1)))/std_power
        x_valid = (x_valid-x_valid.mean(axis=1).reshape((-1,1)))/std_power

        y_train = y_train/self.mean_on_power
        y_valid = y_valid/self.mean_on_power

        x_train = np.nan_to_num(x_train)
        y_train = np.nan_to_num(y_train)
        y_train = np.multiply(y_train, x_train_mask)
        x_train_mask = x_train_mask.astype(bool)
        #np.putmask(y_train, x_train_mask, 0)

        x_valid = np.nan_to_num(x_valid)
        y_valid = np.nan_to_num(y_valid)
        y_valid = np.multiply(y_valid, x_valid_mask)
        x_valid_mask = x_valid_mask.astype(bool)
        #np.putmask(y_valid, x_valid_mask, 0)

        LOCAL_DATA_DIR = '../..'
        model_dir = LOCAL_DATA_DIR + '/models/'
        appliance_model_name = "fully_conv_net" + '_{1}_{0}'.format(self.appliance, self.typeOfPower)
        model_path = model_dir + appliance_model_name + '.h5'

        stats_dir = LOCAL_DATA_DIR + '/stats/'
        stats_path = stats_dir + appliance_model_name +'.csv'

        
        single_model = model
        model.compile(loss='mean_squared_error', optimizer='adam')
        print('Training samples: {0}'.format(x_train.shape))

        # train model

        model_checkpoint = MyModelCheckpoint(model_path, monitor='val_loss', save_best_only=False,
                                     save_weights_only=True)
        model_checkpoint.model = single_model
        callbacks = [model_checkpoint,
                     EarlyStopping(monitor='val_loss', patience=15),
                     CSVLogger(stats_path),
                     ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=10, verbose=1,
                                      mode='auto', cooldown=0, min_lr=0.000001)]

        history = model.fit(x=[x_train, x_train_mask],
                  y=y_train,
                  epochs=num_epochs,
                  batch_size=batch_size,
                  verbose=1,
                  callbacks=callbacks,
                  validation_data=([x_valid, x_valid_mask], y_valid),
                  shuffle=True)



        history_dict = history.history
        print(history_dict.keys())
        #plt.clf()
        #plt.plot(history.history['accuracy'])
        #plt.plot(history.history['val_accuracy'])
        #plt.title('model accuracy')
        #plt.ylabel('accuracy')
        #plt.xlabel('epoch')
        #plt.legend(['train', 'val'], loc='upper left')
        #plt.savefig('../../plots/' + appliance_model_name + '_accuracy_plot.pdf')
        plt.clf()
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title(self.appliance + " - " + self.typeOfPower.split('_')[1] + ' model loss', fontsize = 13)
        plt.ylabel('loss', fontsize = 12)
        plt.xlabel('epoch', fontsize = 12)
        plt.legend(['train', 'val'], loc='upper left')
        plt.savefig('../../plots/' + appliance_model_name + '_loss_plot.pdf')

        self.model = model
        return self

    def load(self, model_prefix, model_path='./', window_length=None):
        """ Load a model. """

        model = self.init_model(window_length)
        model.load_weights(model_path / Path(f'{model_prefix}{self.appliance}{self.typeOfPower}.h5'))
        self.model = model
        return self

    def save(self, model_prefix, model_path='./'):
        """ Save the model weights. """

        model.save_weights(model_path / Path(f'{model_prefix}{self.appliance}{self.typeOfPower}.h5'))

    def predict(self, test_data, homename, testedPower, pad_final_window=True, mask_underspecified_targets=True,
                drop_masked_windows=False, verbose=0, **kwargs):
        """ Predict outputs for a single data frame. """

        '''
        if self.model is None:
            raise ValueError('Before prediction, a model must first be loaded or trained.')
        if isinstance(df, pd.Series):
            df = df.to_frame()
        if not isinstance(df, pd.DataFrame):
            raise ValueError('The input must be a pd.DataFrame.')
        '''

        # Prepare the input data
        x_test, x_test_mask, y_test, x_time = self.prepare_data(test_data,
                                      pad_final_window=pad_final_window,
                                      mask_underspecified_targets=mask_underspecified_targets,
                                      drop_masked_windows=drop_masked_windows,
                                      verbose=verbose,
                                      **kwargs)
        print("MAX: ", np.nanmax(y_test))
        # Predict and scale
        mean_power = self.appliance_stats[self.appliance_stats['Unnamed: 0']==self.typeOfPower.split('_')[1]].mean_on_power.values[0]
        std_power = self.appliance_stats[self.appliance_stats['Unnamed: 0']==self.typeOfPower.split('_')[1]].std_on_power.values[0]
        # Normalise the values
        x_test_norm = (x_test-x_test.mean(axis=1).reshape((-1,1)))/std_power
        x_test_norm = np.nan_to_num(x_test_norm)
        x_test_mask = x_test_mask.astype(bool)
        predictions = self.model.predict([x_test_norm, x_test_mask], verbose=verbose)
        predictions *= self.mean_on_power

        #x_test_mask = x_test_mask.reshape((x_test_mask.shape[0], x_test_mask.shape[1]*x_test_mask.shape[2]))
        predictions[~x_test_mask] = np.nan
        predictions[predictions < 0] = 0

        # Align the predictions with the inputs
        offset = self.get_output_offset()

        results = pd.DataFrame(
            index=pd.Index(pd.to_datetime(x_time.flatten(), unit='s'), name='time'),
            data={'mains': x_test[:, offset[0]:-offset[1]].flatten(),
                  'predictions': predictions.flatten(),
                  'ground_truth': y_test.flatten()})

        results.dropna(inplace=True)
        appliance_model_name = "fully_conv_net" + '_{1}_{0}_tested-{2}'.format(self.appliance, self.typeOfPower, testedPower)
        pred_store_name = '../../predictions/' + homename +'/' + appliance_model_name + '_results.csv'
        results.to_csv(pred_store_name)

        #return pd.Series(predictions, index=index, name='predictions')

    def prepare_data(self, data, verbose=0, **kwargs):
        """ Prepare the data from multiple homes and sensors. """

        main_inputs = []
        targets_masks = []
        all_targets = []
        time = []
        for n, df in enumerate(data):
            if not isinstance(df, pd.DataFrame):
                raise ValueError('The data input for each home must be a pd.DataFrame.')
            if verbose > 1:
                print(f'  Processing input {n+1} {df.shape}', flush=True)
            ts = df.iloc[:, 0]
            tgt = None if df.shape[1] == 1 else df.iloc[:, 1]
            i, m, t, tm = self.prepare_one_dataset(ts, tgt, verbose=verbose, **kwargs)
            main_inputs.append(i)
            targets_masks.append(m)
            if t is not None:
                all_targets.append(t)
            time.append(tm)

        if verbose > 1:
            print(f'  Concatenating inputs', flush=True)
        main_input = np.concatenate(main_inputs)
        targets_mask = np.concatenate(targets_masks)
        targets = None if len(all_targets) == 0 else np.concatenate(all_targets)
        timet = np.concatenate(time)
        if verbose:
            print(f'  main_input: {main_input.shape}')
            print(f'  targets_mask: {targets_mask.shape}')
            if targets is not None:
                print(f'  targets: {targets.shape}')

        return main_input, targets_mask, targets, timet

    def prepare_one_dataset(self, ts, tgt=None, drop_masked_windows=True,
                            verbose=0, **kwargs):
        """ Prepare a single dataset. """

        if not (ts.index.freq == self.frequency):
            raise ValueError(f'The data input must be at {self.frequency} frequency. Got {ts.index.freq}.')
        min_length = self.get_minimum_input_length()
        if len(ts) < min_length:
            raise ValueError(f'The data input must have at least {min_length} elements.')

        windows = self.arrange_windowed_data(ts, tgt, verbose=verbose, **kwargs)
        main_input, targets_mask, targets, time = windows

        # Optionally drop windows where all targets are masked, for faster training
        if drop_masked_windows:
            if verbose > 1:
                print(f'    Dropping masked windows', flush=True)
            valid_windows = targets_mask.any(axis=1)
            print(valid_windows)
            main_input = main_input[valid_windows]
            targets_mask = targets_mask[valid_windows]
            if targets is not None:
                targets = targets[valid_windows]

        if verbose > 1:
            print(f'    Replacing NaNs', flush=True)
        print(main_input.shape)
        print(targets.shape)
        print(targets_mask.shape)
        return main_input, targets_mask, targets, time

    def arrange_windowed_data(self, ts, targets,
                              pad_final_window=True,
                              mask_underspecified_targets=True,
                              verbose=0, **kwargs):
        """ Arrange the data into windows. """

        def generate_mask(ts, offset):
            # Mask out targets that have any missing input data
            gaps = ts.isnull().astype(np.int8)
            conv = np.convolve(gaps.values, np.ones(sum(offset) + 1), mode='same')
            mask = 1 - (conv > 0)
            # Drop unwanted values
            return mask[offset[0]:-offset[1]]

        
        offset = self.get_output_offset()
        n_targets = len(ts) - sum(offset)
        n_windows = 1 + (n_targets - 1) // self.output_window_length
        n_padding = n_windows * self.output_window_length - n_targets

        values = ts.values
        if n_padding > 0:
            if verbose > 1:
                print(f'    Padding inputs', flush=True)
            values = np.append(values, [np.nan] * n_padding)
        # Stack the data into windows
        if verbose > 1:
            print(f'    Stacking input', flush=True)
        main_input = np.vstack([values[i*self.output_window_length: i*self.output_window_length + self.input_window_length]
                      for i in range(n_windows)])
        
        # Generate the targets mask, using 0 and 1
        if verbose > 1:
            print(f'    Generating mask', flush=True)

        targets_mask = np.zeros(len(main_input) * self.output_window_length)
        targets_mask[:n_targets] = generate_mask(ts, offset)
        print(targets_mask.shape)

        # Reshape into windows
        targets_mask = targets_mask.reshape((n_windows, self.output_window_length))

        # Stack the targets, if any, into windows
        if targets is not None:
            if verbose > 1:
                print(f'    Stacking targets', flush=True)
            # Skip the targets that are in the offset window
            targets = targets[offset[0]:-offset[1]].values
            # Pad targets as needed
            if n_padding > 0:
                if verbose > 1:
                    print(f'    Padding targets', flush=True)
                targets = np.append(targets, [np.nan] * n_padding)
            # Reshape into windows
            targets = targets.reshape((n_windows, self.output_window_length))
            # Mask out missing targets
            targets_mask = np.multiply(targets_mask, 1 - np.isnan(targets))

        nan_pad =pd.date_range(freq='10S', start=ts.index[-1], periods=n_padding + 1, inclusive='right')
        tm = ts.index
        tm = tm.append(nan_pad)
        time = tm[offset[0]:-offset[1]].values
        time = time.reshape((n_windows, self.output_window_length))
        
        return main_input, targets_mask, targets, time

    def get_minimum_input_length(self):
        """ Get the minimum input length for the model. """

        return (self.initial_filter_size - 1) + (2 ** (self.num_dilation_layers + 2) - 4) + 1

    def get_output_offset(self):
        """ Get the output offset lengths (left and right). """

        # Compute the reduction in size from input to output layer
        reduction = self.input_window_length - self.output_window_length
        offset_right = reduction // 2
        # Set offsets to match desired alignment
        offset_left = reduction - offset_right
        return offset_left, offset_right
