# All rights reserved. University of Edinburgh, United Kingdom
# SMILE Project, 2023

import argparse
import os
import sys

parser = argparse.ArgumentParser(description='Train a fully convolutional model')
parser.add_argument('--appliance', help='Name of appliance to predict', default=['kettle', 'microwave', 'dishwasher'])
parser.add_argument('--layers', help='Number of dilation layers', type=int, default=0)
parser.add_argument('--rate', help='Sampling rate', type=int, default=10)
parser.add_argument('--window', help='Window size: 0 for smallest, -ve defines output window, +ve defines input window, None for default', type=int, default=3275)
parser.add_argument('--lag', help='Lag between target and last input. -1 puts target in the middle (default)', type=int, default=-1)
parser.add_argument('--log', help='Location to log statistics', default='../../lit_stats/')
parser.add_argument('--models', help='Location to save intermediate models', default='../../lit_models/')
parser.add_argument('--train', help='Train the model', action='store_true', default=True)
parser.add_argument('--epochs', help='Maximum number of epochs for training', type=int, default=0)
parser.add_argument('--batch', help='Batch size for training', type=int, default=0)
parser.add_argument('--real-power', help='Use real power (default is apparent power)', action='store_true', default=False)
parser.add_argument('--no-pad', help='Drop the final window instead of padding it', action='store_true', default=False)
parser.add_argument('--verbose', help='Increase or decrease verbosity of output (default 1)', type=int, default=1)
parser.add_argument('--gpus', help='Number of GPUs to use (default 0)', type=int, default=0)

if __name__ != '__main__':
    sys.argv = []

args = parser.parse_args()

# check GPU preferences before importing tensorflow
if args.gpus < 1:
    # don't use GPUS
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
if args.gpus < 5:
    # reduce the verbosity of tensorflow
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import numpy as np
import pandas as pd
import random as python_random
import tensorflow as tf

from pathlib import Path


from litFCNModel import DEFAULT_BATCH_SIZE, DEFAULT_NUM_EPOCHS, DEFAULT_NUM_DILATION_LAYERS
from litFCNModel import MAINS_REAL, MAINS_APPARENT
from litFCNModel import NILM_FCN_Model

if args.layers == 0:
    args.layers = DEFAULT_NUM_DILATION_LAYERS

if args.epochs == 0:
    args.epochs = DEFAULT_NUM_EPOCHS

if args.batch == 0:
    args.batch = DEFAULT_BATCH_SIZE

SEED_VALUE = 20220815

for gpu in tf.config.list_physical_devices("GPU"):
    tf.config.experimental.set_memory_growth(gpu, True)

# Fix random seed values
def reset_seeds(seed_value):
    os.environ['PYTHONHASHSEED'] = str(seed_value)
    np.random.seed(seed_value)
    python_random.seed(seed_value)
    tf.random.set_seed(seed_value)

# Create the output directories as needed
def make_output_dirs(args):
    if make_output_dir(args.log, 'Log', args.verbose):
        return make_output_dir(args.models, 'Model', args.verbose)
    else:
        return False

def make_output_dir(dir_name, dir_type, verbose):
    filepath = Path.cwd() / dir_name
    if not filepath.exists():
        filepath.mkdir(parents=True, exist_ok=True)
    if verbose:
        print(f'{dir_type} dir is {str(filepath)}')
    return filepath.exists()

MAINS = MAINS_REAL if args.real_power else MAINS_APPARENT

def load_home_data(home_set, typeOfPower, appliance, freq):
    data_array = []
    direc = '../../../model_data_CillianLike/' + appliance + '/' + typeOfPower.split('_')[1] + '/' + home_set
    for filename in os.listdir(direc):
        f = os.path.join(direc, filename)
        if os.path.isfile(f):
            readings = pd.read_csv(f)
            readings.set_index('time', inplace=True)
            readings.index = pd.to_datetime(readings.index)
            readings = readings.sort_index()
            readings = readings.asfreq(freq)
            data_array.append(readings)
    return data_array

def main():
    verbose = args.verbose
    appliances = args.appliance
    if verbose:
        print(f'Training with {vars(args)}')

    for appliance in appliances:
        # load data from training homes
        training_data = load_home_data('train', MAINS, appliance, f'{args.rate}S')
                                       
        # load data from validation homes (don't augment)
        validation_data = load_home_data('dev', MAINS, appliance, f'{args.rate}S')

        print('Done loading data...')

        # create the model
        model = NILM_FCN_Model(appliance,
                               num_dilation_layers=args.layers,
                               frequency=f'{args.rate}S',
                               typeOfPower=MAINS)

        print('initialized model. Start training...')

        # train the model
        model.train(training_data,
                    validation_data,
                    num_epochs=args.epochs,
                    batch_size=args.batch,
                    window_length=args.window,
                    pad_final_window=(not args.no_pad),
                    logging_path=args.log,
                    model_path=args.models,
                    verbose=verbose)
        for testedPower in ['apparent']: #, 'real']:
            direc = '../../../model_data_CillianLike/' + appliance + '/' + testedPower + '/' + 'test'
            for filename in os.listdir(direc):
                homename = filename.split('_')[0]
                f = os.path.join(direc, filename)
                if os.path.isfile(f):
                    readings = pd.read_csv(f)
                    readings.set_index('time', inplace=True)
                    readings.index = pd.to_datetime(readings.index)
                    readings = readings.sort_index()
                    readings = readings.asfreq(f'{args.rate}S')
                    model.predict([readings], homename, testedPower)

    if verbose:
        print(f'Done', flush=True)

if __name__ == '__main__':
    # execute only if run as a script
    reset_seeds(SEED_VALUE)
    main()
