# MInf2 Alex Chelba

Order of operations:
- in preprocessing folder, run data_loader_new.py (you will need to create a folder structure manually, I don't deal with that in code)
- in preprocessing folder, run explorer.py (I hope it works, it hasn't been tested. You can see all my work in DataExplorer.ipynb, from which I tried to extract the bits that were used in the end. Any compilation issues may be due to differences in compiling ipynb and py files. Again, you will need to create some folder structures manually, otherwise you will face compilation errors).
- in models/FCN folder, run trainFCNModel.py (if everything compiled successfully until here, then there should be no trouble).

I can polish code further once I'm done with my exams.
