from activationDetector import ActivationDetector
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os

# TO USE THE METHODS IN THIS FILE, ONE MUST HAVE RUN THE data_loader_new.py FILE BEFORE

# method which appends row of data to dataframe
def append_row(df, row):
    return pd.concat([
                df, 
                pd.DataFrame([row], columns=row.index)]
           ).reset_index(drop=True)

# method which constructs a dataframe containing the amount of data, expressed in the number of days, for each appliance in each home
def getDataAmountInDaysAllAppliancesAllHomes():
    directory = '../../preprocessed_data'
    appliances = ['homeid', 'mains_apparent', 'mains_real', 'dishwasher', 'microwave', 'kettle']
    statdf = pd.DataFrame(columns = appliances)
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        homename = filename.split('_')[0]
        statdf = append_row(statdf, pd.Series({'homeid': homename, 'mains_apparent': 0, 'mains_real': 0, 'dishwasher': 0, 'microwave': 0, 'kettle': 0}))
        if os.path.isfile(f):
            df = pd.read_csv(f)
            for app in list(df):
                statdf.loc[statdf.homeid == homename, app] = df[app].count()/(6*60*24)

# method which constructs a dataframe containing the amount of data, expressed in the number of days, for appliance in each home, when the specific type of power are available as well
def getHomesPerApplianceAndPower(typeOfPower, appliance):
    directory = '../../preprocessed_data'
    appliances = [typeOfPower, appliance]
    print(appliances)
    statdf = pd.DataFrame(columns = ['homeid'].extend(appliances))
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        homename = filename.split('_')[0]
        statdf = append_row(statdf, pd.Series({'homeid': homename, typeOfPower: 0, appliance: 0}))
        if os.path.isfile(f):
            df = pd.read_csv(f)
            if set(appliances).issubset(set(list(df))):
                df.set_index('time', inplace=True)
                df.index = pd.to_datetime(df.index)
                df = df.loc[(df[typeOfPower].notna()) & (df[appliance].notna()), :]
                for app in appliances:
                    statdf.loc[statdf.homeid == homename, app] = df[app].count()/(6*60*24)
    return statdf

# method which constructs a dataframe containing the amount of data, expressed in the number of days, for appliance in each home, when apparent&real power are available as well
def getHomesPerAppliance(appliance):
    directory = '../../preprocessed_data'
    appliances = ['mains_apparent', 'mains_real']+[appliance]
    print(appliances)
    statdf = pd.DataFrame(columns = ['homeid'].extend(appliances))
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        homename = filename.split('_')[0]
        statdf = append_row(statdf, pd.Series({'homeid': homename, 'mains_apparent': 0, 'mains_real': 0, appliance: 0}))
        if os.path.isfile(f):
            df = pd.read_csv(f)
            if set(appliances).issubset(set(list(df))):
                df.set_index('time', inplace=True)
                df.index = pd.to_datetime(df.index)
                df = df.loc[(df['mains_apparent'].notna()) & (df['mains_real'].notna()) & (df[appliance].notna()), :]
                for app in appliances:
                    statdf.loc[statdf.homeid == homename, app] = df[app].count()/(6*60*24)
    return statdf

# build data folder of data from homes when appliance and both types of power are available
def getDatasetEqualPowerAmountsPerAppliance(statdf, appliance):
    directory = '../../preprocessed_data'
    saveDir = '../../preprocessed_data_equalPowerAmountsAnom/' + appliance
    homes = statdf.homeid.values
    for home in homes:
        filename = home + '_sampledData.csv'
        f = os.path.join(directory, filename)
        homename = filename.split('_')[0]
        if os.path.isfile(f):
            df = pd.read_csv(f)
            df.set_index('time', inplace=True)
            df.index = pd.to_datetime(df.index)
            print(df.head())
            df = df.loc[(df.mains_apparent.notna())&(df.mains_real.notna())&(df[appliance].notna()), :]
            df.loc[:, ['mains_apparent', 'mains_real', appliance]].to_csv(saveDir+'/'+filename)

# method which builds dataframe of months during which data is available for appliance in each home
def getMonthsperHomeforAppliance(appliance):
    directory = '../../preprocessed_data_equalPowerAmountsAnom/' + appliance
    yearMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    statdf = pd.DataFrame(columns = ['homeid'].extend(yearMonths))
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        homename = filename.split('_')[0]
        statdf = append_row(statdf, pd.Series({'homeid': homename, 'January': False, 'February': False, 'March': False, 'April': False, 'May': False, 'June': False, 'July': False, 'August': False, 'September': False, 'October': False, 'November': False, 'December': False}))
        if os.path.isfile(f):
            df = pd.read_csv(f)
            df.set_index('time', inplace=True)
            df.index = pd.to_datetime(df.index)
            df = df.resample('M')[appliance].sum()
            months = df.index.month.values
            for month in months:
                statdf.loc[statdf.homeid == homename, yearMonths[month-1]] = True
    return statdf

# build final data folder for paper model
def constructModelData(appliance):
    directory = '../../preprocessed_data_equalPowerAmountsAnom/' + appliance
    saveDir = '../../model_dataAnom/' + appliance + '/'
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        homename = filename.split('_')[0]
        if os.path.isfile(f):
            df = pd.read_csv(f)
            df.set_index('time', inplace=True)
            df.index = pd.to_datetime(df.index)
            typeOfPower = 'apparent'
            dfApp = df.loc[:, ['mains_apparent', appliance]]
            dfApp.rename(columns={"mains_apparent": "power", appliance: "appliance"}, inplace = True)
            dfApp.to_csv(saveDir+typeOfPower + '/' + filename)
            typeOfPower = 'real'
            dfReal = df.loc[:, ['mains_real', appliance]]
            dfReal.rename(columns={"mains_real": "power", appliance: "appliance"}, inplace = True)
            dfReal.to_csv(saveDir+typeOfPower + '/' + filename)

# build final folder for literature model
def constructModelDataLiterature(appliance):
    directory = '../../preprocessed_data'
    saveDir = '../../model_data_literatureComparison/' + appliance + '/'
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        homename = filename.split('_')[0]
        if os.path.isfile(f):
            df = pd.read_csv(f)
            if set(['mains_apparent', appliance]).issubset(set(list(df))):
                df.set_index('time', inplace=True)
                df.index = pd.to_datetime(df.index)
                typeOfPower = 'apparent'
                dfApp = df.loc[:, ['mains_apparent', appliance]]
                dfApp.rename(columns={"mains_apparent": "power", appliance: "appliance"}, inplace = True)
                dfApp.to_csv(saveDir+typeOfPower + '/' + filename)

# upscale apparent power data, save in data folder
def solveApparentRealPowerOrderKeepData():
    directory = '../../preprocessed_data'
    saveDir = '../../preprocessed_data_anomalySolvedUpscale'
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        homename = filename.split('_')[0]
        if os.path.isfile(f):
            df = pd.read_csv(f)
            if set(['mains_apparent', 'mains_real']).issubset(set(list(df))):
                df.set_index('time', inplace=True)
                df.index = pd.to_datetime(df.index)
                #print(df.count())
                df_diff = pd.Series(name='upscaler')
                smoother = 0.00001
                df['upscaler'] = df['mains_real']/df['mains_apparent']
                df = df.loc[df['upscaler']<=1.5, :]
                maxx = df['upscaler'].max()
                df['mains_apparent'] = df['mains_apparent'] * maxx
                df.drop(columns = ['upscaler'], inplace = True)
                df.to_csv(saveDir + '/' + filename)
                #print(df_diff[df_diff>1.0].count()/df_diff.count())

# method which get the activations count for each appliance per set (train, validation, test)
def getActivationCounterPerSet():
    rules_file = 'gt_rules.json'
    for appliance in ['kettle', 'microwave', 'dishwasher']:
        activation_detector = ActivationDetector(appliance, rulesfile=rules_file, sample_rate=10)
        print(appliance)
        for home_set in ['train', 'dev', 'test']:
            print(home_set)
            direc = '../../model_dataAnom/' + appliance + '/' + 'apparent' + '/' + home_set
            counter = 0
            for filename in os.listdir(direc):
                f = os.path.join(direc, filename)
                if os.path.isfile(f):
                    readings = pd.read_csv(f)
                    readings.set_index('time', inplace=True)
                    readings.index = pd.to_datetime(readings.index)
                    readings = readings.sort_index()
                    activations = activation_detector.get_activations(readings.appliance)
                    counter+=activations.count()
            print(counter)

if __name__ == '__main__':
    ##################
    allAppliancesStatDF = getDataAmountInDaysAllAppliancesAllHomes()
    allAppliancesStatDF.loc['Total #days', :] = allAppliancesStatDF.sum()
    print(allAppliancesStatDF)
    ##################
    kettleStatDF = getHomesPerAppliance('kettle')
    microwaveStatDF = getHomesPerAppliance('microwave')
    dishwasherStatDF = getHomesPerAppliance('dishwasher')
    kettleStatDF = kettleStatDF[kettleStatDF.kettle>=1].sort_values(by=['kettle'], ascending=False)
    microwaveStatDF = microwaveStatDF[microwaveStatDF.microwave>=1].sort_values(by=['microwave'], ascending=False)
    dishwasherStatDF = dishwasherStatDF[dishwasherStatDF.dishwasher>=1].sort_values(by=['dishwasher'], ascending=False)
    ##################
    kettleStatDFApparent = getHomesPerApplianceAndPower('mains_apparent', 'kettle')
    kettleStatDFReal = getHomesPerApplianceAndPower('mains_real', 'kettle')
    microwaveStatDFApparent = getHomesPerApplianceAndPower('mains_apparent','microwave')
    microwaveStatDFReal = getHomesPerApplianceAndPower('mains_real','microwave')
    dishwasherStatDFApparent = getHomesPerApplianceAndPower('mains_apparent', 'dishwasher')
    dishwasherStatDFReal = getHomesPerApplianceAndPower('mains_real', 'dishwasher')
    ##################
    s1 = kettleStatDF.kettle.sum()
    s2 = kettleStatDFApparent.kettle.sum()
    print('Kettle difference in amounts of power apparent to apparent&real: ', s2-s1)
    s1 = microwaveStatDF.microwave.sum()
    s2 = microwaveStatDFApparent.microwave.sum()
    print('Microwave difference in amounts of power apparent to apparent&real: ', s2-s1)
    s1 = dishwasherStatDF.dishwasher.sum()
    s2 = dishwasherStatDFApparent.dishwasher.sum()
    print('Dishwasher difference in amounts of power apparent to apparent&real: ', s2-s1)
    s1 = kettleStatDF.kettle.sum()
    s2 = kettleStatDFReal.kettle.sum()
    print('Kettle difference in amounts of power real to apparent&real: ', s2-s1)
    s1 = microwaveStatDF.microwave.sum()
    s2 = microwaveStatDFReal.microwave.sum()
    print('Microwave difference in amounts of power real to apparent&real: ', s2-s1)
    s1 = dishwasherStatDF.dishwasher.sum()
    s2 = dishwasherStatDFReal.dishwasher.sum()
    print('Dishwasher difference in amounts of power real to apparent&real: ', s2-s1)
    ##################
    print(microwaveStatDF.microwave.cumsum())
    print(kettleStatDF.kettle.cumsum())
    print(dishwasherStatDF.dishwasher.cumsum())
    ##################
    getDatasetEqualPowerAmountsPerAppliance(kettleStatDF, 'kettle')
    getDatasetEqualPowerAmountsPerAppliance(microwaveStatDF, 'microwave')
    getDatasetEqualPowerAmountsPerAppliance(dishwasherStatDF, 'dishwasher')
    for appl in ['kettle', 'dishwasher', 'microwave']:
        constructModelData(appl)
    ##################
    getActivationCounterPerSet()