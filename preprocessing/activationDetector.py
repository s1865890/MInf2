import numpy as np
import pandas as pd
import datetime as dt
import gc
import json
import re
from electric import submeter

class ActivationDetector:

    def __init__(self, appliance, rulesfile='gt_rules.json', sample_rate=1):
        
        self.rulesfile=rulesfile
        
        with open(rulesfile) as data_file:
            rules = json.load(data_file)['rules']

        self.rule = None
        for rule in rules:
            pattern = re.compile(rule["appliance"])
            if not (pattern.match(appliance) is None):
                self.rule = rule
                break

        assert self.rule is not None
        self.appliance = appliance

        self.min_off_duration=int(self.rule["min_off_duration"])
        self.min_on_duration=int(self.rule["min_on_duration"])
        self.max_on_duration=int(self.rule["max_on_duration"])
        self.on_power_threshold=int(self.rule["on_power_threshold"])
        
        # minumum activation energy in joules
        
        if "min_energy" in self.rule:
            self.min_energy = int(self.rule["min_energy"])
        else:
            self.min_energy = 0

        self.sample_rate = sample_rate

    def get_activations(self, readings):
        """Get start, end and energy (joules) of appliance activations"""
        
        
        buffer = dt.timedelta(seconds=self.min_off_duration+self.sample_rate*2)
        start_time = readings.index[0]
        end_time = readings.index[-1]

        readings.loc[start_time - buffer] = 0
        readings.loc[end_time + buffer] = 0
        

        readings = (readings
                    .resample('{0}S'.format(self.sample_rate))
                    .fillna('nearest', 1)
                    .fillna(0))

        end = readings.index[-1]
        on_offs = submeter().get_ons_and_offs(readings.rename('values'), end, None,
                                            min_off_duration=self.min_off_duration,
                                            min_on_duration=self.min_on_duration,
                                            max_on_duration=self.max_on_duration,
                                            on_power_threshold=self.on_power_threshold)
        
        starts = on_offs.time[on_offs.state_change=='on'].values
        if on_offs.iloc[-1].state_change=='on':
            starts = starts[:-1]
        
        ends = on_offs.time[on_offs.state_change=='off'].values
        if on_offs.iloc[0].state_change=='off':
            ends = ends[1:]
        
        activations = pd.DataFrame({'start':starts, 'end':ends})
        if activations.shape[0] > 0:
            activations['energy'] = activations.apply(lambda r: readings[r.start:r.end].sum(),
                                                      axis=1) * self.sample_rate
        else:
            activations['energy'] = []
            
        activations = activations[activations.energy > self.min_energy]
        
        return activations