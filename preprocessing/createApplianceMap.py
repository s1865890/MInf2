import pandas as pd
from metadata import MetaData

def createMap():
    appliance_map = pd.DataFrame(columns = ['homeid', 'kettle', 'microwave', 'dishwasher'])
    mtd = MetaData().gold_homes()
    mtd2 = MetaData().sensor_merged()
    dishwasherhomes = mtd2[mtd2.appliancetype == 'dishwasher'].homeid.unique()
    dsh = [x for x in dishwasherhomes if x in mtd.unique()]
    kettlehomes = mtd2[mtd2.appliancetype == 'kettle'].homeid.unique()
    kh = [x for x in kettlehomes if x in mtd.unique()]
    microwavehomes = mtd2[mtd2.appliancetype == 'microwave'].homeid.unique()
    print(mtd2.loc[mtd2.appliancetype == 'kettle', ['homeid', 'status_sensor']])
    mwh = [x for x in microwavehomes if x in mtd.unique()]
    for home in mtd.unique():
        hasKettle = 1 if home in kh else 0
        hasMicrowave = 1 if home in mwh else 0
        hasDishwasher = 1 if home in dsh else 0
        df2 = {'homeid': [home], 'kettle': [hasKettle], 'microwave': [hasMicrowave], 'dishwasher': [hasDishwasher]}
        dff = pd.DataFrame.from_dict(df2)
        appliance_map = pd.concat([appliance_map, pd.DataFrame.from_dict(df2)], ignore_index = True)
    appliance_map = appliance_map.sort_values('homeid')
    appliance_map.to_csv('appliance_map.csv')

if __name__ == '__main__':
    createMap()