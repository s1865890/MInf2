import h5py
import math
import os
import sys
import tempfile

import pandas as pd

from pathlib import Path

from API.IdealDataInterface import IdealDataInterface

# define appliances of interest
APPLIANCES = ['kettle', 'microwave', 'dishwasher']

# define data source
IDEAL_DATA_DIR = os.environ.get('IDEAL_DATA_DIR') or '/afs/inf.ed.ac.uk/group/ideal/DataSharing/PublicReleaseCandidates/'
SENSOR_DATA_DIR = Path(Path(IDEAL_DATA_DIR) / 'IDEALsensordata/csv')

# initialise the IDEAL data API
IDI = IdealDataInterface(SENSOR_DATA_DIR)

# load data for all homes of interest, preprocess and save data in csv file for each home
def load_home_data(homes, sampling_rate=10, augment=0, verbose=0, **kwargs):
    base_rate = 1
    subtype = ['electric-combined', 'mains']
    subtype.extend(APPLIANCES)
    df_data = []
    for homeid in homes:
        homename = 'home'+str(homeid)
        info = IDI.view(homeid=homeid, subtype=subtype)
        # see for which appliances of interest there is data in home
        wanted = set(subtype).intersection(set(info.subtype))
        # if the home has real power mains data
        if 'mains' in wanted:
            print(f'Loading home {homeid}')
            df = load_resampled_data(homeid, wanted, sampling_rate, verbose=verbose, **kwargs)
            df.rename(columns={'electric-combined':'mains_apparent', 'mains':'mains_real'}, inplace = True)
            df.to_csv('preprocessed_data/' + homename + '_sampledData.csv')

# preprocess data and save it in a dataframe
def load_resampled_data(homeid, wanted, sampling_rate, verbose=0, **kwargs):
    wanted_list = list(wanted)
    data = [(app, load_resampled_appliance_data(homeid, app, sampling_rate, verbose=verbose, **kwargs))
                for app in wanted_list]
    ans = pd.DataFrame()
    for (app, df) in data:
        ans[app] = df[app]
    return ans

# preprocess specific appliance data from specific home with homeid
def load_resampled_appliance_data(homeid, appliance, sampling_rate, force_reload=0, verbose=0, **kwargs):
    description = f'resampled {appliance} data'
    df_appliance = load_appliance_data(homeid, appliance, force_reload=force_reload-1, verbose=verbose)
    df = resample_appliance_data(df_appliance, appliance, sampling_rate, verbose=verbose)
    return df

# load appliance data from home
def load_appliance_data(homeid, appliance, force_reload=0, save_to_main_cache=True, verbose=0):
    description = f'{appliance} data'
    data_loader = IDI.load(homeid=homeid, subtype=appliance)
    # Use the sensor IDs to label distinct appliances of the same type
    df = pd.DataFrame({appliance: ts for (ts, desc) in data_loader})
    if verbose:
        print('Done.', flush=True)
    return df

# resample, forward fill and downsample to required sampling rate by taking the mean
def resample_appliance_data(df, appliance, sampling_rate, verbose=0):
    base_rate = 1
    limit = get_ffill_limit(appliance)
    df = df.asfreq(f'{base_rate}S').ffill(limit=limit)
    if sampling_rate > base_rate:
        df = df.resample(f'{sampling_rate}S', origin='epoch').mean()
    return df

# resample 1 hour for appliances or 1 minute for real and apparent power mains data
def get_ffill_limit(appliance):
    if appliance in APPLIANCES:
        return 60*60
    return 60

if __name__ == '__main__':
    homes = [ 61,  62,  63,  65,  73,  90,  96, 106, 105, 136, 128, 139, 140, 145, 146, 168, 169, 171, 162, 175, 208, 212, 225, 228, 227, 231, 238, 242, 249, 255, 262, 264, 263, 266, 268, 259, 276, 311, 328]
    load_home_data(homes)
