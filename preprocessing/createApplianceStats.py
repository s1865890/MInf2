import numpy as np
import pandas as pd
import os

def getMean(meanA, meanB, countA, countB):
	return ((countA * meanA) + (countB * meanB))/(countA+countB)

def getVar(varA, varB, meanA, meanB, countA, countB):
	return ((((countA - 1)*varA) + ((countB-1)*varB))/(countA+countB-1)) + ((countA * countB *(meanA-meanB)*(meanA-meanB))/((countA+countB)*(countA+countB-1)))

def getApplianceStats():
	ans = pd.DataFrame(columns = ['mean_on_power', 'std_on_power'])
	getAppliance = True
	for typeOfPower in ['real', 'apparent']:
		getMains = True
		appliancePowerMean = 0
		applianceVar = 0
		applianceCount = 0
		threshDict = {'kettle':1000, 'dishwasher':50, 'microwave':200}
		for appliance in ['kettle', 'microwave', 'dishwasher']:
			meanPower = 0
			varPower = 0
			countPower = 0
			powerDf = np.array([])
			applianceDf = np.array([])
			direc = '../../model_data/' + appliance + '/' + typeOfPower + '/' + 'train'
			for filename in os.listdir(direc):
				f = os.path.join(direc, filename)
				df = pd.read_csv(f)
				df.set_index('time', inplace=True)
				df.index = pd.to_datetime(df.index)
				if getMains:
					powerDf = np.append(powerDf, df[df['power']>100]['power'].values)
				if getAppliance:
					applianceDf = np.append(applianceDf, df[df['appliance']>threshDict[appliance]]['appliance'].values)
			if getMains:
				print(np.mean(powerDf))
				ans.loc[typeOfPower, 'mean_on_power'] = np.mean(powerDf)
				ans.loc[typeOfPower, 'std_on_power'] = np.std(powerDf)
				getMains = False
			if getAppliance:
				#print(np.mean(applianceDf))
				ans.loc[appliance, 'mean_on_power'] = np.mean(applianceDf)
				ans.loc[appliance, 'std_on_power'] = np.std(applianceDf)
		getAppliance = False
	ans.to_csv('appliance_stats.csv')

if __name__ == '__main__':
	getApplianceStats()